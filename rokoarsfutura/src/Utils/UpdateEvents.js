import { getWeekKey, splitDate } from './TransformData';

export function updateAfterDelete(events, setEvents, eventId, dateKey){
  const filteredArray = events[dateKey].filter(e => e.id !== eventId);
  const newArray = JSON.parse(JSON.stringify(events));

  if(filteredArray.length === 0)
    delete newArray[dateKey];
  else
    newArray[dateKey] = filteredArray;

   setEvents(newArray);
}

export function updateAfterAdd(events, setEvents, data, selected){
  const keys = Object.keys(events);
  const key = (selected === '30' ? getWeekKey(new Date(data.start.dateTime)).toString() : splitDate(data.start.dateTime));

                          //Razlika datuma u danima
  if((new Date(data.start.dateTime) - new Date()) /(1000 * 60 * 60 * 24) <= selected && new Date(data.start.dateTime) >= new Date()){
    let newArray = JSON.parse(JSON.stringify(events));

    if(keys.includes(key)){
      newArray[key].push(data);
      newArray[key].sort((e1, e2) => new Date(e1.start.dateTime) - new Date(e2.start.dateTime));
    }
    else{
      newArray[key] = [data];
      newArray = sortKeys(newArray, selected)
    }
    setEvents(newArray);
  }
}

function sortKeys(array, selected){
  const sorted = {};
  if(selected !== '30'){
    Object.keys(array).sort((e1,e2) => new Date(e1) - new Date(e2)).forEach(key => sorted[key] = array[key]);
  }
  else{
    Object.keys(array).sort((e1,e2) => new Date(e1.split(',')[1]) - new Date(e2.split(',')[0])).forEach(key => sorted[key] = array[key]);
  }
  return sorted;
}
