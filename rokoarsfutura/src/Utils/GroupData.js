import { getWeekKey, splitDate } from './TransformData';

export function groupEvents(events, setEvents, selected){
  const now = new Date();
  now.setHours(now.getHours()+2);
  
  let key;
  const groupedEvents = events.reduce((groupedEvents, event) => {
    if(event.start.dateTime >= now.toISOString()){
      (selected === '30' ? key = getWeekKey(new Date(event.start.dateTime)) : key = splitDate(event.start.dateTime))
        if(!groupedEvents[key])
          groupedEvents[key] = [];
        groupedEvents[key].push(event);
    }
    return groupedEvents;
  }, {});

  setEvents(groupedEvents);
}
