export function eventURL(n, calendarId){
  const date = new Date();

  const url = new URL(`https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events`);
  url.searchParams.append('orderBy', 'startTime');
  url.searchParams.append('timeMin',  date.toISOString());
  date.setDate(date.getDate() + n);
  url.searchParams.append('timeMax',  date.toISOString());
  url.searchParams.append('singleEvents', true);

  return url;
}

export function loginURL(){
  const url = new URL('https://accounts.google.com/o/oauth2/v2/auth');

  url.searchParams.append('client_id', '117904747043-nktqul6qg3eh48guja5h83okkage6ppm.apps.googleusercontent.com');
  url.searchParams.append('redirect_uri', 'http://localhost:3000/main');
  url.searchParams.append('response_type', 'token');
  url.searchParams.append('scope', 'https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/calendar.readonly');

  return url;
}
