export function getWeekKey(date){
  const firstDayOfWeek = new Date(date.getFullYear(), date.getMonth(), date.getDate() + (date.getDay() === 0 ? -5 : 2)-date.getDay());
  const lastDayOfWeek = new Date(date.getFullYear(), date.getMonth(), date.getDate() + (date.getDay() === 0 ? 1 : 8)-date.getDay());

  return [splitDate(firstDayOfWeek.toISOString()),splitDate(lastDayOfWeek.toISOString())];
}

export function makeNewEvent(eventData){
  return {
    'summary': eventData.summary,
    'start': {
      'dateTime': new Date(`${eventData.date}T${eventData.start}`).toISOString(),
      'timeZone': Intl.DateTimeFormat().resolvedOptions().timeZone,
    },
    'end': {
      'dateTime': new Date(`${eventData.date}T${eventData.end}`).toISOString(),
      'timeZone': Intl.DateTimeFormat().resolvedOptions().timeZone,
    }
  };
}

export function splitDate(date){
  return date.split('T')[0];
}
