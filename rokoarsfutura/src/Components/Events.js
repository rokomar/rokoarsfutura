import React from 'react';
import styles from '../Styles/Event.module.css';
import { splitDate } from '../Utils/TransformData';

export function Events(props){
  return (
    <ul>
    <span className={styles.listHeader}>{props.date}</span>
      {props.events.map(event =>
          <li key={event.id} className={styles.list}>
             <span className={styles.name}>{event.summary}</span>
             <span className={styles.date}>{splitDate(event.start.dateTime)}</span>
             {event.start.dateTime.split('T')[1].split('+')[0]}-{event.end.dateTime.split('T')[1].split('+')[0]}
             <button className={styles.delete} onClick={() => props.handleDelete(event.id, props.date)}>X</button>
          </li>
      )}
    </ul>
  );
}
