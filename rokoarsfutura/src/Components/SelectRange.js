import React from 'react';

export function SelectRange(props){
  return(
    <div>
      <label htmlFor="selection">Show events in: </label>
      <select onChange={props.handleChange} defaultValue='7' id="selection">
        <option value='1'>1 day</option>
        <option value='7'>7 days</option>
        <option value='30'>30 days</option>
      </select>
    </div>
  );
}
