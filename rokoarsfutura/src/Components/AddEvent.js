import React from 'react';
import useForm from 'react-hook-form';
import styles from '../Styles/AddEvent.module.css';
import { splitDate } from '../Utils/TransformData';

export function AddEvent(props){

  const {register, handleSubmit, errors} = useForm();

  return (
      <form onSubmit={handleSubmit(props.handleAddEvent)} className={styles.form}>
      {Object.keys(errors).length !== 0 && <span className={styles.error}>All fields are required</span>}
        <input name="summary"
               placeholder="Event"
               ref={register({
                 required: true
               })}
               className={styles.components}/>
        <input type="date"
               name="date"
               id="date"
               ref={register({
                 required: true
               })}
               min={splitDate(new Date().toISOString())}
               defaultValue={splitDate(new Date().toISOString())}
               className={styles.components}/>

        <div className={styles.components}>
          <label htmlFor="start">Start: </label>
          <input type="time"
                 name="start"
                 ref={register({
                   required: true
                 })}
                 className={styles.start}
                 onChange={(e) => document.getElementById('end').setAttribute('min', e.target.value)}/>

          <label htmlFor="end">End: </label>
          <input type="time"
                 name="end"
                 id="end"
                 ref={register({
                   required: true
                 })}/>

        </div>
        <button type="submit" className={styles.button}>Add event</button>
      </form>
  );
}
