import React, {useState, useEffect} from 'react';

import { GetCalendar } from '../Services/CalendarService';
import { GetEventsWeek, GetEventsDay, GetEventsYear, DeleteEvent, AddNewEvent } from '../Services/EventService';

import { groupEvents} from '../Utils/GroupData';
import { updateAfterDelete, updateAfterAdd } from '../Utils/UpdateEvents';
import { makeNewEvent } from '../Utils/TransformData';

import { Events } from '../Components/Events';
import { AddEvent } from '../Components/AddEvent';
import { SelectRange } from '../Components/SelectRange';

import styles from '../Styles/MainContainer.module.css';

export function MainContainer(props){

  const [events, setEvents] = useState({});
  const [selected, setSelected] = useState('7');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async function(){
      const url = window.location.href;
      const token = url.match(/access_token=([^&]*)/gm).toString().split('=')[1];
      localStorage.setItem('token', token);

      const calendarId = await GetCalendar()
                                .catch(err => handleAuthError(err));
      localStorage.setItem('calendarId', calendarId);
      showEvents(selected);
    })();
  },[])

  const showEvents = (selected) => {
    (async function(){
      const calendarId = localStorage.getItem('calendarId');
      let events;

      setLoading(true);
      switch(selected){
        case '1':
          events = await GetEventsDay(calendarId)
                              .catch(err => handleAuthError(err));
          break;
        case '7':
          events = await GetEventsWeek(calendarId)
                            .catch(err => handleAuthError(err));
          break;
        default:
          events = await GetEventsYear(calendarId)
                              .catch(err => handleAuthError(err));
      }
      (events ? groupEvents(events, setEvents, selected) : setEvents({}));
      setLoading(false);
    })();
  }

  const handleEventDelete = (eventId, dateKey) => {
    DeleteEvent(localStorage.getItem('calendarId'), eventId)
      .then(() => {
          updateAfterDelete(events, setEvents, eventId, dateKey);
        }).catch(err => handleAuthError(err));
  }

  const handleAddEvent = (eventData) => {
    AddNewEvent(localStorage.getItem('calendarId'), makeNewEvent(eventData))
      .then((response) => {
          updateAfterAdd(events, setEvents, response, selected);
      }).catch(err => {
        handleAuthError();
      });
  }

  const handleChange = (e) => {
    setSelected(e.target.value);
    showEvents(e.target.value);
  }

  const handleAuthError = (err) => {
    if(err.message === '401')
      props.history.replace("/");
    console.log(err);
  }

  return (
    <div className={styles.container}>
      <SelectRange handleChange={handleChange} />

      {loading ? <span>Loading...</span>
               : (Object.keys(events).length === 0 ? <span>No events to show</span>
                                                   : Object.keys(events).map(date => <div key={date}>
                                                                                        <Events events={events[date]} date={date} handleDelete={handleEventDelete}/>
                                                                                     </div>))}

      <AddEvent handleAddEvent={handleAddEvent} />
    </div>
  );
}
