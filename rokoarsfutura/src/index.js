import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { MainContainer } from './Containers/MainContainer';
import { loginURL } from './Utils/URL';

function App(){
  return(
    <BrowserRouter>
      <Route exact path='/' component={() => (window.location.href=loginURL())} />
      <Route path="/main" component={MainContainer} />
    </BrowserRouter>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
