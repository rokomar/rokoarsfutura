export function get(url){
  return fetch(url, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Authorization': ` Bearer ${localStorage.getItem('token')}`,
    }
  }).then(response => {
    if(!response.ok)
      throw new Error(response.status);
    return response.json();
  })
}

export function deleteAPI(url){
  return fetch(url, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Authorization': ` Bearer ${localStorage.getItem('token')}`,
    }
  }).then(response => {
    if(!response.ok)
      throw new Error(response.status);
  })
}

export function post(url, data){
  return fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': ` Bearer ${localStorage.getItem('token')}`,
    },
    body: JSON.stringify(data)
  }).then(response => {
    if(!response.ok)
      throw new Error(response.status);
    return response.json();
  })
}
