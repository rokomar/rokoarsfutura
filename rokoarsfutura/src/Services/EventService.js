import { get, deleteAPI, post } from './api';
import { eventURL } from '../Utils/URL';

export function GetEventsWeek(calendarId){
  return get(eventURL(7, calendarId))
    .then(events => {
      return events.items;
    });
}

export function GetEventsDay(calendarId){
  return get(eventURL(1, calendarId))
    .then(events => {
      return events.items;
    });
}

export function GetEventsYear(calendarId){
  return get(eventURL(30, calendarId))
    .then(events => {
      return events.items;
    });
}

export function DeleteEvent(calendarId, eventId){
  return deleteAPI(new URL(`https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events/${eventId}`));
}

export function AddNewEvent(calendarId, eventData){
  return post(new URL(`https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events`),eventData);
}
