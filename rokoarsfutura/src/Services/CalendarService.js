import { get } from './api';

export function GetCalendar(){
  return get('https://www.googleapis.com/calendar/v3/users/me/calendarList')
    .then(calendars => {
      return calendars.items[0].id;
    });
}
